package ru.nsu.ccfit.ermakova;

import org.xbill.DNS.*;

import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class ProxyServer {
    private static final int BUFFER_SIZE = 16384;
    private static final int DNS_SERVER_PORT = 53;

    private int port;
    private ServerSocketChannel serverSocket;
    private DatagramChannel udpChann;
    private SelectionKey serverKey;
    private SelectionKey udpKey;
    private Selector selector;
    private Map<SelectionKey, SelectionKey> connectedKeys;
    private Map<SelectionKey, SocksConnection> connections;
    private Map<String, String> domains;
    private FileWriter writer;
    InetAddress localAddress;

    public ProxyServer(int port) {
        this.port = port;
    }

    private void openChannels() throws IOException {
        ResolverConfig cfg = ResolverConfig.getCurrentConfig();
        String resolver = cfg.server();
        InetAddress dnsIP = InetAddress.getByName(resolver);

        selector = Selector.open();

        udpChann = DatagramChannel.open();
        udpChann.configureBlocking( false );
        udpChann.connect(new InetSocketAddress(dnsIP, DNS_SERVER_PORT));
        udpKey = udpChann.register(selector, SelectionKey.OP_READ);

        serverSocket = ServerSocketChannel.open();
        localAddress = InetAddress.getLocalHost();
        serverSocket.bind(new InetSocketAddress(localAddress, port));
        serverSocket.configureBlocking(false);
        serverKey = serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        writer = new FileWriter("out.txt", false);
    }

    private void allocateMemory() {
        connectedKeys = new LinkedHashMap<>();
        connections = new LinkedHashMap<>();
        domains = new LinkedHashMap<>();
    }

    public void start() throws IOException {
        openChannels();
        allocateMemory();

        while (true) {
            try {
                selector.select();
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> iter = selectedKeys.iterator();
                while (iter.hasNext()) {
                    SelectionKey key = iter.next();
                    if (key == serverKey) {
                        openNewConnection();
                    } else if (key == udpKey) {
                        processUDP();
                    } else dataReceived(key);
                    iter.remove();
                }
            } catch (IOException e) {
            }
        }
    }
    private void openNewConnection() throws IOException {
        SocketChannel client = serverSocket.accept();
        client.configureBlocking(false);
        SelectionKey clientKey = client.register(selector, SelectionKey.OP_READ);
        connections.put(clientKey, new SocksConnection(SocksConnectStage.WAITING_FOR_AUTH_METHODS));
    }

    private void processUDP() throws IOException {
        ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
        udpChann.read(buffer);
        // Await response from DNS server
        DataInputStream din = new DataInputStream(new ByteArrayInputStream(buffer.array()));
        din.readShort();
        din.readShort();
        din.readShort();
        din.readShort();
        din.readShort();
        din.readShort();
        StringBuilder rcvDomain = new StringBuilder();
        int recLen = 0;
        boolean firstRecord = true;
        while ((recLen = din.readByte()) > 0) {
            byte[] record = new byte[recLen];
            for (int i = 0; i < recLen; i++) {
                record[i] = din.readByte();
            }
            if (!firstRecord)
                rcvDomain.append(".");
            firstRecord = false;
            rcvDomain.append(new String(record, "UTF-8"));
        }
        din.readShort();
        din.readShort();
        din.readShort();
        din.readShort();
        din.readShort();
        din.readInt();
        short addrLen = din.readShort();
        if (addrLen != 4) return;
        StringBuilder ipAddressBuilder = new StringBuilder();
        for (int i = 0; i < addrLen; i++ ) {
            byte b = din.readByte();
            ipAddressBuilder.append("" + String.format("%d", (b & 0xFF)));
            if (i < addrLen - 1) ipAddressBuilder.append(".");
        }
        domains.put(rcvDomain.toString(), ipAddressBuilder.toString());

        Iterator<Map.Entry<SelectionKey, SocksConnection>> entries = connections.entrySet().iterator();
        while (entries.hasNext()) {
            Map.Entry<SelectionKey, SocksConnection> entry = entries.next();
            if (entry.getValue().getDomain() == null) continue;
            if (entry.getValue().getDomain().equals(rcvDomain.toString())) {
                try {
                    answerAccessGranted(entry.getKey());
                    checkIPAddress(entry.getKey());
                    entries.remove();
                } catch (IOException e) {
                } catch (UnresolvedAddressException e) {
                }
            }
        }
    }

    private void dataReceived(SelectionKey key) throws IOException {
        if (keyInConnections(key) && ((key.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ)) {
            SocksConnection state = connections.get(key);
            SocketChannel channel = (SocketChannel)key.channel();
            ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
            int size = -1;
            try {
                size = channel.read(buffer);
            } catch (IOException e) {
                channel.close();
                connections.remove(key);
            }
            if (size == -1) return;
            if (size == 0) return;
            switch (state.getStage()) {
                case UNDEFINED:
                    break;
                case WAITING_FOR_AUTH_METHODS:
                    parseSocksAuthMethods(key, size, buffer);
                    break;
                case WAITING_FOR_CONNECTION_REQUEST:
                    parseConnectionRequest(key, size, buffer);
                    break;
            }
        } else if (keyInDataTransfer(key) && ((key.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ)) {
            SelectionKey keyTo = connectedKeys.get(key);
            SocketChannel fromChannel = (SocketChannel)key.channel();
            SocketChannel toChannel = (SocketChannel)keyTo.channel();
            try {
                ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
                fromChannel.read(buffer);
                buffer.flip();
                toChannel.write(buffer);
            } catch (IOException e) {
                fromChannel.close();
                toChannel.close();
                connectedKeys.remove(key);
                connectedKeys.remove(keyTo);
            }
        }
    }

    private void sendResolvingRequest(SelectionKey key) throws IOException {
        String domain = connections.get(key).getDomain();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        DataOutputStream dos = new DataOutputStream(baos);

        // *** Build a DNS Request Frame ****
        // Identifier: A 16-bit identification field generated by the device that creates the DNS query.
        // It is copied by the server into the response, so it can be used by that device to match that
        // query to the corresponding reply received from a DNS server. This is used in a manner similar
        // to how the Identifier field is used in many of the ICMP message types.
        dos.writeShort(0x1234);
        dos.writeShort(0x0100); // Write Query Flags
        dos.writeShort(0x0001); // Question Count: Specifies the number of questions in the Question section of the message.
        dos.writeShort(0x0000); // Answer Record Count: Specifies the number of resource records in the Answer section of the message.
        dos.writeShort(0x0000); // Authority Record Count: Specifies the number of resource records in the Authority section of the message. (“NS” stands for “name server”)
        dos.writeShort(0x0000); // Additional Record Count: Specifies the number of resource records in the Additional section of the message.
        String[] domainParts = domain.split("\\.");
        for (int i = 0; i<domainParts.length; i++) {
            byte[] domainBytes = domainParts[i].getBytes("UTF-8");
            dos.writeByte(domainBytes.length);
            dos.write(domainBytes);
        }
        dos.writeByte(0x00); // No more parts
        dos.writeShort(0x0001); // Type 0x01 = A (Host Request)
        dos.writeShort(0x0001); // Class 0x01 = IN
        byte[] dnsFrame = baos.toByteArray();
        ByteBuffer buffer = ByteBuffer.wrap(dnsFrame);

        // *** Send DNS Request Frame ***
        udpChann.write(buffer);
    }

    private void answerAuthChoice(SelectionKey key, boolean isSuccess) throws IOException {
        SocketChannel channel = (SocketChannel)key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(2);
        buffer.put((byte) 0x05); // SOCKS version, 1 byte (0x05 for this version)
        buffer.put((byte) (isSuccess ? 0x00 : 0xFF)); // chosen authentication method, 1 byte, or 0xFF if no acceptable methods were offered
        buffer.flip();
        channel.write(buffer);
    }

    private void answerRefuse(SelectionKey key, byte answerCode) throws IOException {
        SocketChannel channel = (SocketChannel)key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(7);
        buffer.put((byte) 0x05); // SOCKS protocol version, 1 byte (0x05 for this version)
        buffer.put(answerCode); // status, 1 byte: 0x07: command not supported / protocol error or 0x08: address type not supported
        buffer.put((byte) 0x00); // reserved, must be 0x00, 1 byte
        buffer.put((byte) 0x03); // address type, 1 byte: 0x03: Domain name
        buffer.put((byte) 0x00); // server bound address of 1 byte of name length followed by 1–255 bytes the domain name
        buffer.put((byte) 0x00); // server bound port number in a network byte order, 2 bytes
        buffer.put((byte) 0x00); // server bound port number in a network byte order, 2 bytes
        buffer.flip();
        channel.write(buffer);
    }

    private void answerAccessGranted(SelectionKey key) throws IOException {
        SocketChannel channel = (SocketChannel)key.channel();
        ByteBuffer buffer = ByteBuffer.allocate(10);
        buffer.put((byte) 0x05); // SOCKS protocol version, 1 byte (0x05 for this version)
        buffer.put((byte) 0x00); // status, 1 byte: 0x00: request granted
        buffer.put((byte) 0x00); // reserved, must be 0x00, 1 byte
        buffer.put((byte) 0x01); // address type, 1 byte: 0x01: IPv4 address or 0x03: Domain name

        byte [] addressBytes = localAddress.getAddress();
        buffer.put(addressBytes[0]);
        buffer.put(addressBytes[1]);
        buffer.put(addressBytes[2]);
        buffer.put(addressBytes[3]);

        byte hiPortByte = (byte) ((port & 0xFF00) >> 8);
        byte lowPortByte = (byte) (port & 0x00FF);

        buffer.put(hiPortByte);
        buffer.put(lowPortByte);
        buffer.flip();
        channel.write(buffer);
    }

    private void parseSocksAuthMethods(SelectionKey key, int size, ByteBuffer buffer) throws IOException {
        byte [] buf = buffer.array();
        if (size != 2 + buf[1]) return;
        if (buf[0] != 0x05) return;
        int n = (int) buf[1];
        if (n + 2 != size) return;
        boolean authFound = false;
        for (int i = 0; i < n; i++) {
            if (buf[2 + i] == 0x00) {
                authFound = true;
                break;
            }
        }
        answerAuthChoice(key, authFound);
        if (!authFound) return;
        connections.get(key).setStage(SocksConnectStage.WAITING_FOR_CONNECTION_REQUEST);
        return;
    }

    private void parseConnectionRequest(SelectionKey key, int size, ByteBuffer buffer) throws IOException {
        if (size < 10) return;
        byte [] buf = buffer.array();
        if (buf[0] != 0x05) return;
        if (buf[1] != 0x01) {
            answerRefuse(key, (byte) 0x07); // command not supported / protocol error
            return;
        }
        if (buf[3] == 0x01) { // address type, 1 byte: 0x01: IPv4 address
            if (size != 10) return;
            byte [] adddressBytes = new byte[4];
            adddressBytes[0] = buf[4];
            adddressBytes[1] = buf[5];
            adddressBytes[2] = buf[6];
            adddressBytes[3] = buf[7];
            byte hiByte = buf[8];
            byte lowByte = buf[9];
            int port = ((((int) hiByte) & 0xFF) << 8) | ((int) (lowByte & 0xFF));
            bindIPAddress(key, adddressBytes, port);
            answerAccessGranted(key);
        } else if (buf[3] == 0x03) { // address type, 1 byte: 0x03: Domain name
            int len = (int) buf[4];
            if (size != len + 7) return;
            byte [] domainBytes = new byte[len];
            for (int i = 0; i < len; i++)
                domainBytes[i] = buf[5 + i];
            String domain = new String(domainBytes, "UTF-8");
            byte hiByte = buf[5 + len];
            byte lowByte = buf[6 + len];
            int port = ((((int) hiByte) & 0xFF) << 8) | ((int) (lowByte & 0xFF));
            connections.get(key).setDomain(domain);
            connections.get(key).setPort(port);
            connections.get(key).setStage(SocksConnectStage.UNDEFINED);
            sendResolvingRequest(key);
        } else answerRefuse(key, (byte) 0x08); // address type not supported
    }

    private void checkIPAddress(SelectionKey key) throws IOException {
        String domain = connections.get(key).getDomain();
        if (domains.containsKey(domain)) {
            String address = domains.get(domain);
            int port = connections.get(key).getPort();
            InetSocketAddress remoteAddress = new InetSocketAddress(address, port);
            if (remoteAddress == null | remoteAddress.getAddress() == null) {
                domains.remove(key);
                connections.remove(key);
                return;
            }
            SocketChannel remote = SocketChannel.open(remoteAddress);
            remote.configureBlocking(false);
            SelectionKey remoteKey = remote.register(selector, SelectionKey.OP_READ);
            connectedKeys.put(key, remoteKey);
            connectedKeys.put(remoteKey, key);
        }
    }

    private void bindIPAddress(SelectionKey key, byte [] ip, int port) throws IOException {
        InetSocketAddress remoteAddress = new InetSocketAddress(InetAddress.getByAddress(ip), port);
        if (remoteAddress == null | remoteAddress.getAddress() == null) {
            domains.remove(key);
            connections.remove(key);
            return;
        }
        SocketChannel remote = SocketChannel.open(remoteAddress);
        remote.configureBlocking(false);
        SelectionKey remoteKey = remote.register(selector, SelectionKey.OP_READ);
        connectedKeys.put(key, remoteKey);
        connectedKeys.put(remoteKey, key);
        connections.remove(key);
    }

    private boolean keyInConnections(SelectionKey key) {
        return connections.containsKey(key);
    }

    private boolean keyInDataTransfer(SelectionKey key) {
        return connectedKeys.containsKey(key);
    }
}
