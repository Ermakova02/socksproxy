package ru.nsu.ccfit.ermakova;

public enum SocksConnectStage {
    UNDEFINED("UNDEFINED"),
    WAITING_FOR_AUTH_METHODS("WAITING_FOR_AUTH_METHODS"),
    WAITING_FOR_CONNECTION_REQUEST("WAITING_FOR_CONNECTION_REQUEST");

    private String value;

    SocksConnectStage(final String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.getValue();
    }
}
